# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Toussis Manolis <manolis@koppermind.homelinux.org>, 2008.
# Stelios <sstavra@gmail.com>, 2011.
# Dimitris Kardarakos <dimkard@gmail.com>, 2014.
msgid ""
msgstr ""
"Project-Id-Version: kdevclassbrowser\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-11-09 00:15+0000\n"
"PO-Revision-Date: 2014-03-19 20:51+0200\n"
"Last-Translator: Dimitris Kardarakos <dimkard@gmail.com>\n"
"Language-Team: Greek <kde-i18n-el@kde.org>\n"
"Language: el\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 1.5\n"

#: classbrowserplugin.cpp:70 classbrowserplugin.cpp:121 classwidget.cpp:35
#, fuzzy, kde-format
#| msgid "Classes"
msgctxt "@title:window"
msgid "Classes"
msgstr "Κλάσεις"

#: classbrowserplugin.cpp:73
#, fuzzy, kde-format
#| msgid "Find in &Class Browser"
msgctxt "@action"
msgid "Find in &Class Browser"
msgstr "Αναζήτηση στον περιηγητή &κλάσεων"

#: classwidget.cpp:72
#, fuzzy, kde-format
#| msgid "S&earch:"
msgctxt "@label:textbox"
msgid "S&earch:"
msgstr "Α&ναζήτηση:"

#: classwidget.cpp:89
#, fuzzy, kde-format
#| msgid "Class Browser"
msgctxt "@info:whatsthis"
msgid "Class Browser"
msgstr "Περιηγητής κλάσεων"

#~ msgid "All projects classes"
#~ msgstr "Κλάσεις για όλα τα έργα"

#~ msgid "Base classes"
#~ msgstr "Βασικές κλάσεις"

#~ msgid "Derived classes"
#~ msgstr "Παράγωγες κλάσεις"

#~ msgid "Classes in project %1"
#~ msgstr "Κλάσεις στο έργο %1"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Stelios"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "sstavra@gmail.com"

#~ msgid ""
#~ "This plugin provides a browsable model of the currently parsed classes "
#~ "and other items."
#~ msgstr ""
#~ "Αυτό το πρόσθετο προσφέρει ένα μοντέλο περιήγησης των αναλυμένων κλάσεων "
#~ "και άλλων αντικειμένων."

#~ msgid "Browser for all known classes"
#~ msgstr "Περιηγητής όλων των γνωστών κλάσεων"

#, fuzzy
#~| msgid "Open &Declaration"
#~ msgid "Show &Declaration"
#~ msgstr "Άνοιγμα &δήλωσης"

#, fuzzy
#~| msgid "Open De&finition"
#~ msgid "Show De&finition"
#~ msgstr "Άνοιγμα &ορισμού"

#~ msgid "Global Functions"
#~ msgstr "Καθολικές συναρτήσεις"

#~ msgid "Global Variables"
#~ msgstr "Καθολικές μεταβλητές"

#, fuzzy
#~| msgid "Unknown object!"
#~ msgid "Unknown object."
#~ msgstr "Άγνωστο αντικείμενο!"

#~ msgid "Code View"
#~ msgstr "Προβολή κώδικα"
